#!/usr/bin/env bash
# Function to update a single Git repository
update_repo() { 
	local repo_path="$1"
	if [[ -d "$repo_path/.git" ]]; then
		(
			cd "$repo_path" || return
			echo "Updating repository: $repo_path" 
            		if git pull; then
				echo "Update successful: $repo_path"
			else
				echo "Update failed: $repo_path" >&2
			fi
		)
	else
		echo "Not a Git repository: $repo_path" >&2
	fi
}
# Export the function for xargs
export -f update_repo

# Find Git repositories and update them in parallel
find . -type d -name ".git" -exec dirname {} \; | xargs -P 0 -I {} bash -c 'update_repo "{}"'
