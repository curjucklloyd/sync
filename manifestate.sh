#!/bin/bash

packages = "tmux git neofetch flatpak"

packages_deb = "build-essential manpages-dev"
packages_rpm = "gcc-c++"
packages_gnome = "gnome-tweaks gnome-tweak-tool gnome-extensions gnome-shell-extension-pop-shell gnome-software-plugin-flatpak"
flatpaks = "org.gnome.World.PikaBackup org.telegram.desktop org.onlyoffice.desktopeditors com.discordapp.Discord org.gabmus.hydrapaper org.videolan.VLC com.vscodium.codium io.dbeaver.DBeaverCommunity com.gitlab.newsflash org.gimp.GIMP com.github.IsmaelMartinez.teams_for_linux org.mozilla.Thunderbird flathub org.mozilla.firefox"

gnome_exts_links = "https://extensions.gnome.org/extension/615/appindicator-support/ https://extensions.gnome.org/extension/3193/blur-my-shell/ https://extensions.gnome.org/extension/3240/add-to-desktop/ https://extensions.gnome.org/extension/779/clipboard-indicator/ https://extensions.gnome.org/extension/947/dim-on-battery-power/ https://extensions.gnome.org/extension/2236/night-theme-switcher/"

if [ -x "$(command -v apt-get)" ]; then
    echo ".deb-based distro detected"
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get autoremove -y
    ### !! NEED MORE DEB !! ###

elif [ -x "$(command -v dnf)" ]; then    
    echo ".rpm-based distro detected"

    # DNF config
    sudo echo "\n#Post-install modifications" >> /etc/dnf/dnf.conf
    sudo echo "fastestmirror=True" >> /etc/dnf/dnf.conf
    sudo echo "max_parallel_downloads=10" >> /etc/dnf/dnf.conf
    sudo echo "defaultyes=True" >> /etc/dnf/dnf.conf

    sudo dnf update

    # RPM-fusion
    sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf install -y rpmfusion-free-release
    sudo dnf install -y rpmfusion-free-release-tainted
    sudo dnf install -y rpmfusion-nonfree-release-tainted

    # Codecs
    sudo dnf swap -y ffmpeg-free ffmpeg --allowerasing
    sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
    sudo dnf groupinstall -y multimedia
    sudo dnf groupupdate -y multimedia
    sudo dnf groupupdate -y sound-and-video
    sudo dnf install -y intel-media-driver
    sudo dnf install -y libva-intel-driver
    sudo dnf install -y rpmfusion-nonfree-release-tainted
    sudo dnf --repo=rpmfusion-nonfree-tainted install -y "*-firmware"

    # AppStream metadata
    sudo dnf groupupdate -y core



    sudo dnf update
    sudo dnf remove -y firefox
    sudo dnf install -y $packages_rpm $packages
    
    
    #Flatpak
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak install flathub $flatpaks

    sudo dnf autoremove 

    if [ $XDG_CURRENT_DESKTOP=="GNOME" ]; then
        sudo dnf install -y $packages_gnome
        firefox >/dev/null 2>&1 & $gnome_exts_links
    fi
else    
   echo "Unsupported distribution"
   exit 1 
fi



echo "Package updates complete"