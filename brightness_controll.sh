#!/bin/bash

# in percent
LOW_BRIGHTNESS=25
HIGH_BRIGHTNESS=100
SLEEPTIME=3 # in seconds

provider=$(ls /sys/class/backlight/)
if [ -z "$provider" ]; then
    echo "No backlight provider found!"
    exit 1
elif [ -z "$(ls /sys/class/power_supply)" ]; then
    echo "No power supply info found!"
    exit 1
fi

power_supply_path="/sys/class/power_supply/ACAD/online"
brightness_path="/sys/class/backlight/$provider/brightness"
max_brightness_path="/sys/class/backlight/$provider/max_brightness"

set_brightness() {
    local percent=$1
    local max_brightness=$(cat $max_brightness_path)
    local new_brightness=$(($percent * $max_brightness / 100))
    local current_brightness=$(cat $brightness_path)

    # Apply new brightness only if it's less than the current when on battery, or always when on AC
    if [[ $percent -eq $LOW_BRIGHTNESS && $new_brightness -lt $current_brightness ]] || [[ $percent -eq $HIGH_BRIGHTNESS ]]; then
        echo $new_brightness > $brightness_path
    fi
}

monitor_power() {
    local current_status
    local last_status=$(cat $power_supply_path)
    
    while true; do
        read current_status < $power_supply_path
        if [ "$current_status" != "$last_status" ]; then
            if [ "$current_status" == "0" ]; then
                set_brightness $LOW_BRIGHTNESS
            else
                set_brightness $HIGH_BRIGHTNESS
            fi
            last_status=$current_status
        fi

        sleep $SLEEPTIME
    done
}

monitor_power

